package SymbolTable;

public class SymbolTable {
	
	/*tabela simbola za "language scope"
	u ovom slucaju tu pripadaju samo tipovi*/
	private SymbolNode types;
	
	/* tabela simbola za oblast vazenja programa */
	private SymbolNode variables;
	
	public SymbolTable( )
	{
		types = new Type( "unknown", Type.UNKNOWN, null);
		types = new Type( "char", Type.CHAR, types );
		types = new Type( "integer", Type.INT, types );
		types = new Type( "real", Type.REAL, types );
		variables = null;
	}
	
	public boolean addVar( String name, Type type )
	{
		Variable existing = this.getVar( name );
		if ( existing != null )
			return false;
		variables = new Variable( name, type, variables );
		return true;
	}
	
	public boolean addVar( String name, Type type, boolean field )
	{
		Variable existing = this.getVar( name );
		if ( existing != null )
			return false;
		variables = new Variable( name, type, field, variables );
		return true;
	}
	
	public Variable getVar( String name )
	{
		Variable current = (Variable) variables;
		while ( current != null && 
				current.name.compareTo( name ) != 0 )
			current = (Variable) current.next;
		return ( Variable ) current;
	}
	
	public Type getType(String typeName)
	{
		SymbolNode current = types;
		while ( current != null && 
				current.name.compareTo( typeName ) != 0 )
			current = current.next;
		return ( Type ) current;
	}
	
	public SymbolNode getVariables()
	{
		return variables;
	}

}
