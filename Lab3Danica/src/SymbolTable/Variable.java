package SymbolTable;

import java.util.ArrayList;

public class Variable extends SymbolNode {
	
	public int last_def;
	public int last_use;
	public boolean field;
	public Object value;
	public ArrayList dimensions;
	public ArrayList indexes;
	
	public Variable( String name, 
			Type type,
			SymbolNode next )
	{
		super( name, SymbolNode.VARIABLE, type, next );
		last_def = -1;
		last_use = -1;
		field = false;
		value = null;
	}
	
	public Variable( String name,
			Type type,
			boolean field,
			SymbolNode next )
	{
		super( name, SymbolNode.VARIABLE, type, next );
		last_def = -1;
		last_use = -1;
		this.field = field;
		value = null;
	}	
}
