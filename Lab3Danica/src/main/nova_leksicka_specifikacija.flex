// import section
import java_cup.runtime.*;

%%
// declaration section
%class MPLexer
%cup
%line
%column
%eofval{
	return new Symbol( sym.EOF );
%eofval}

//states
%state COMMENT
//macros
slovo = [a-zA-Z]
cifra = [0-9]
hexCifra = [0-9a-zA-F]
octCifra = [0-7]
%%
// rules section
\(\* { yybegin( COMMENT ); }
<COMMENT>\*\) { yybegin( YYINITIAL ); }
<COMMENT>. { ; }
[\t\n\r ] { ; }
\{ 					{ return new Symbol( sym.LEFTPAR ); }
\} 					{ return new Symbol( sym.RIGHTPAR ); }
\[ 					{ return new Symbol( sym.LEFTBRACKET ); }
\] 					{ return new Symbol( sym.RIGHTBRACKET ); }
\( 					{ return new Symbol( sym.LEFTBRACE ); }
\) 					{ return new Symbol( sym.RIGHTBRACE ); }

//operators
\+ 					{ return new Symbol( sym.PLUS ); }
\- 					{ return new Symbol( sym.MINUS ); }
\* 					{ return new Symbol( sym.MUL ); }
\/ 					{ return new Symbol( sym.DIV ); }

//separators
; 					{ return new Symbol( sym.DOTCOMMA ); }
, 					{ return new Symbol( sym.COMMA ); }
= 					{ return new Symbol( sym.ASSIGN ); }

//keywords
"main" 				{ return new Symbol( sym.MAIN ); }
"int" 				{ return new Symbol( sym.INT ); }
"char"				{ return new Symbol( sym.CHAR ); }
"real" 				{ return new Symbol( sym.REAL ); }
"foreach" 			{ return new Symbol( sym.FOREACH ); }
"in" 				{ return new Symbol( sym.IN ); }
"read" 				{ return new Symbol( sym.READ ); }
"write" 			{ return new Symbol( sym.WRITE ); }

//id-s
{slovo}({slovo}|{cifra})* { return new Symbol( sym.ID, yyline, yytext() ); }

//constants
'[^]' { return new Symbol( sym.CHARCONST, new Character( yytext().charAt(1) )); }
0x{hexCifra}+ { return new Symbol( sym.INTCONST, new Integer( yytext() ) ); }
0\.{cifra}+(E(\+|\-){cifra}+)? { return new Symbol( sym.REALCONST, new Double ( yytext() ) ); }
0{octCifra} { return new Symbol( sym.INTCONST, new Integer( yytext() ) ); }
{cifra}+ { return new Symbol( sym.INTCONST, new Integer( yytext() ) ); }

//error symbol
. 	{ System.out.println( "ERROR: " + yytext() ); }