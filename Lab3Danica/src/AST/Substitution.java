package AST;

public class Substitution extends BinaryExpression {
	
	public Substitution( Expression l, Expression r ) {
		super( l, r );
	}

	@Override
	protected String opCode() {
		return "Sub";
	}

}
