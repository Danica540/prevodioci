package AST;

import java.io.BufferedWriter;
import java.io.IOException;

import SymbolTable.Variable;

public class ForeachStatement extends Statement {
	private Variable var1;
	private Variable var2;
	private Statement statement;
	
	public ForeachStatement( Variable var1, Variable field, Statement statement ) {
		this.var1 = var1;
		this.var2 = field;
		this.statement = statement;
	}

	@Override
	void translate(BufferedWriter out) throws IOException {
		String var = ASTNode.genVar();
                String petlja=ASTNode.genLab();
		out.write( "\tLoad_Const\t" +  var + ", " + ((Integer)var2.dimensions.get(0)).toString() );
		out.newLine();
		out.write( petlja+":" );
		out.newLine();
		out.write( "\tLoad_Mem\tR1, " + var2.name );
		out.newLine();
		out.write( "\tStore\t\tR1, " + var1.name );
		out.newLine();
		statement.translate( out );
		out.write( "\tSub\t\t " + var + ", 1");
		out.newLine();
		out.write( "\tJumpIfZero\t " + var + ", "+petlja );
		out.newLine();
	}

}
