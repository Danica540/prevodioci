package AST;

public class Division extends BinaryExpression {
	
	public Division( Expression l, Expression r ) {
		super( l, r );
	}

	@Override
	protected String opCode() {
		return "Div";
	}

}
