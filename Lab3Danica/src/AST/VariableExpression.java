package AST;

import java.io.BufferedWriter;
import java.io.IOException;

import SymbolTable.Variable;

public class VariableExpression extends Expression {
	private Variable targetVariable;
	
	public VariableExpression( Variable var ) {
		targetVariable = var;
	}

	@Override
	void translate(BufferedWriter out) throws IOException {
		this.result = targetVariable.name;
	}

}
