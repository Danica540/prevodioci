
public class sym {

	public final static int EOF = 0;
	public final static int MAIN = 1;
	public final static int INT = 2;
	public final static int CHAR = 3;
        public final static int REAL = 4;
	public final static int READ = 5;
	public final static int WRITE = 6;
        public final static int CURLY_BRACES_LEFT = 7;
	public final static int CURLY_BRACES_RIGHT = 8;
        public final static int SEMICOLON = 9;
	public final static int COMMA = 10;
	public final static int DOT = 11;
        public final static int FOR = 12;
        public final static int SQUARE_BRACKETS_LEFT = 13;
	public final static int SQUARE_BRACKETS_RIGHT = 14;
	public final static int CONST = 15;
	public final static int IN = 16;
	public final static int LEFT_BRACKETS = 17;
	public final static int RIGHT_BRACKETS = 18;
	public final static int MUL = 19;
	public final static int PLUS = 20;
	public final static int MINUS = 21;
	public final static int DIVIDE = 22;
        public final static int ASSIGN = 23;
        public final static int ID = 24;
	

}
