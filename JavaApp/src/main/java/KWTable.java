
import java.util.Hashtable;
public class KWTable {

	private Hashtable mTable;
	public KWTable()
	{
		// Inicijalizcaija hash tabele koja pamti kljucne reci
		mTable = new Hashtable();
		mTable.put("main", new Integer(sym.MAIN));
		mTable.put("for", new Integer(sym.FOR));
		mTable.put("int", new Integer(sym.INT));
		mTable.put("char", new Integer(sym.CHAR));
		mTable.put("real", new Integer(sym.REAL));
		mTable.put("in", new Integer(sym.IN));
		mTable.put("read", new Integer(sym.READ));
		mTable.put("write", new Integer(sym.WRITE));
	}
	
	/**
	 * Vraca ID kljucne reci 
	 */
	public int find(String keyword)
	{
		Object symbol = mTable.get(keyword);
		if (symbol != null)
			return ((Integer)symbol).intValue();
		
		// Ako rec nije pronadjena u tabeli kljucnih reci radi se o identifikatoru
		return sym.ID;
	}
	

}
