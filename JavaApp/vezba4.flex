// import section
import java_cup.runtime.Symbol;
%%
// declaration section
%class MPLexer
%cup
%function next_token
%line
%column
%debug
%type Symbol
%eofval{
return new Symbol( sym.EOF);
%eofval}


%{
//extra class members
	public int getLine()
	{
		return yyline;
	}
%}

%{
//dodatni clanovi generisane klase
KWTable kwTable = new KWTable();
Symbol getKW()
{
	return new Symbol( kwTable.find( yytext() ));
}
%}

//states
%state KOMENTAR
//makroi
slovo = [a-zA-Z]
cifra = [0-9]
osnova=([2-9]|1[0-6])

%%

// pravila
\/\*\* { yybegin( KOMENTAR ); }
<KOMENTAR>~"*/" { yybegin( YYINITIAL ); }

[\t\n\r ] { ; }
\( { return new Symbol( sym.LEFT_BRACKETS, yytext() ); }
\) { return new Symbol( sym.RIGHT_BRACKETS, yytext() ); }
\[ { return new Symbol( sym.SQUARE_BRACKETS_LEFT, yytext()  ); }
\] { return new Symbol( sym.SQUARE_BRACKETS_RIGHT, yytext()  ); }
\{ { return new Symbol( sym.CURLY_BRACES_LEFT, yytext()  ); }
\} { return new Symbol( sym.CURLY_BRACES_RIGHT, yytext()  ); }

//operatori
\+ { return new Symbol( sym.PLUS,yytext()  ); }
\* { return new Symbol( sym.MUL,yytext()  ); }
\- { return new Symbol( sym.MINUS, yytext()  ); }
\/ { return new Symbol( sym.DIVIDE, yytext()  ); }
//separatori
; { return new Symbol( sym.SEMICOLON, yytext()  ); }
, { return new Symbol( sym.COMMA, yytext()  ); }
\. { return new Symbol( sym.DOT, yytext()  ); }
= { return new Symbol( sym.ASSIGN, yytext()  ); }

//kljucne reci
{slovo}+ { return getKW(); }
//identifikatori
{slovo}({slovo}|{cifra})* { return new Symbol(sym.ID, yytext() ); }
//konstante
'.' { return new Symbol( sym.CHAR,yytext()  ); }
({osnova}?#)?{cifra}+  { return new Symbol( sym.CONST, yytext()  ); }
({cifra}+)\.({cifra}*(E(\+|\-)?{cifra}+)) { return new Symbol( sym.CONST, yytext()  ); }
//obrada gresaka
. { if (yytext() != null && yytext().length() > 0) System.out.println( "ERROR: " + yytext() ); }
